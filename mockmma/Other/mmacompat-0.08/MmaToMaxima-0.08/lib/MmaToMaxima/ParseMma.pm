package MmaToMaxima::ParseMma;

=begin copyright

COPYRIGHT AND LICENCE

Copyright (C) 2008, John Lapeyre  All rights reserved.

This program is free software; you can redistribute it
and/or modify it only under the terms of the GNU General
Public License version 2 as published by the Free Software
Foundation.

=cut

use strict;
use Parse::RecDescent;
use Data::Dumper;
use Carp;


# For debugging. printing column.
sub Parse::RecDescent::prcol {
    my (@itempos) = @_;
    for( my $i=1; $i<$#itempos+1; $i++) {
	print " token $i: (" .$itempos[$i]{column}{from} . ",".
	    $itempos[$i]{column}{to} . ")\n"; }}

#  Clean this (way) up!
# process matches from binary operator.
# note that lambda functions are detected here.
sub Parse::RecDescent::bin_item {
    my @list = @_;
#    print "testing '" . $list[0] . "'\n";
#    print "BEFORE: " . Data::Dumper::Dumper(\@list);
    my $n = @list;
    if ( not ref($list[1]) ) { # code never reached i hope
	print "TERMINAL " . Data::Dumper::Dumper(\@list);
	return(\@list);
    }
    my @l2 = @{$list[1]};
    my $n2 = @l2;
    my $rlist;
    if ( $list[0] eq 'success' ) {
	print "Got success\n";
	shift(@list); $rlist = \@list;
    }
    elsif ($n == 3) {
	if ( $list[2] eq  '&' and $list[0] eq 'compound') {
#	    unshift(@{$list[1]}, 'lambda');
	    return ['lambda',$list[1]];
	}
#	print "BEFORE: " . Data::Dumper::Dumper(\@list);
    }
    elsif ( $n2 == 2 ) {
	$rlist = $list[1]; # zero matches, ie only an artificial match to ensure 
                           # precedence
                           # so we are falling through
    }
    elsif ( $n2 < 2 ) {  # one term matched
	$rlist = $list[1]->[0];
    }
    else {
#	print "Stripping delims\n";
        my @t = @{$list[1]}; # strip delimiters from list
	my @ind = (0..$#t/2);
	foreach(@ind) { $_ *= 2 }
	$rlist = [$list[0], [@t[@ind]]];
#	$rlist = \@list;
    }
#    print "AFTER: " . Data::Dumper::Dumper($rlist);
    return $rlist;
}

# process matches from grouping delimeters
sub Parse::RecDescent::group_item {
    [shift(@_),$_[1]];
}

# process matches from 'unit' item (eg. constants, lists, etc.
# Not operator matches.
sub Parse::RecDescent::unit_item {
    shift(@_);
    [@_];
}

# process matches from terminal items. 
sub Parse::RecDescent::term_item {
    [ $_[0], [ $_[1] ] ];
}

# process matches for postfix operators. There are 
# three of these. Can proabably be refactored
sub Parse::RecDescent::post_item { # only for headblock now !
    my (@item) = @_;
    my $nelem = @{$item[2]};
#    print "POST1 STDERR before nelem $nelem: " . Data::Dumper::Dumper(\@item);
    if ($nelem == 0) { 
#    if (@{$item[2]} != 1) { 
	shift(@item);
	pop(@item);
#	print STDERR "POST1 no op : " . Data::Dumper::Dumper($item[0]->[0]);
	$item[0]->[0]
	}
    else { # below kinda works 
#	print STDERR "POST1 before nelem $nelem: dumping all\n" . Data::Dumper::Dumper(\@item);
	my $type = shift(@item);
	my $head = shift(@item);
	$head = $head->[0];

# headblock has as elements a head and a single element (multiple_headblock),
# which hash blocks as elements
#	my $multi = ['multiple_headblock',@item]; # one way to do it
#	my $ret = [$type,[$head,$multi]];  # 

# another way. headblock has as elements a head and one or more blocks
	unshift(@{$item[0]},$head);
	my $ret = [$type,@item]; 

#	print STDERR  "POST1 got op changed :\n" . Data::Dumper::Dumper($ret);
	$ret;
    }
}

sub Parse::RecDescent::post_item2 {
    my (@item) = @_;
#    print STDERR "POST2 before : " . Data::Dumper::Dumper(\@item);
    if (@{$item[2]}>0) { # got inc op
	pop(@item);
#	print STDERR "POST2 got op : " . Data::Dumper::Dumper(\@item);
	[shift(@item),[@item]];
    }
    else { # no inc op
#	print STDERR "POST2 fall through : " . Data::Dumper::Dumper(	$item[1] );
	$item[1] }}


# For 'part'
sub Parse::RecDescent::post_item3 {
    my (@item) = @_;
#    print "POST3 before : " . Data::Dumper::Dumper(\@item);
    if (@{$item[2]}>0) { # got one or more part ops
        my $type = shift(@item);
#	push(@{$item[0]},[$item[1]]);
	unshift(@{$item[1]},$item[0]);
#	print "POST3 after1 : " . Data::Dumper::Dumper(	[$type,$item[1]]);
	[$type,$item[1]];
#	$item[2] = $item[2]->[0];
#	print "POST3 after1 : " . Data::Dumper::Dumper(\@item);
#	[@item];
    }
    else {
#	print "POST3 after2 : " . Data::Dumper::Dumper(	$item[1] );
	$item[1] 
#	[@item]
}}

# process matches for prefix operators. There are 
sub Parse::RecDescent::pre_item {
    my (@item) = @_;
    if (@{$item[1]} == 0) {  # fall through
	$item[2];
    }
    else {
	[shift(@item),[$item[1]]]
    }
}


# uncomment to produce tons of trace info
# $RD_TRACE=1;

our $MmaGrammar =
q{

#  groupings

parentheses_group	:	'(' expr(s? /,/) <commit> ')'   { group_item(@item) }
        |  <error?>

curly_brace_list	:	'{' expr(s? /,/) <commit> '}'  {  group_item(@item) }
        |  <error?>

#empty_curly_brace_list	:	'{' '}'   { group_item(@item) }


square_bracket_block	:      left_square_bracket expr(s? /,/) <commit> ']'   { group_item(@item) }
        |  <error?>

left_square_bracket : '['


# Terminals

identifier     : /[a-zA-Z]\w*/  {  term_item(@item) }

double_colon_identifier     : /[a-zA-Z]\w*::\w*/  {  term_item(@item) }

farg           : /[a-zA-Z]\w*_/ { term_item(@item) }

patvar        : /\#+\d*/ { term_item(@item) }

integer        : /[-+]?\d+/ { term_item(@item) }

float          : /[+-]?(?=\d|\.\d)\d*(\.\d*)([Ee]([+-]?\d+))?(?!\w)/ { term_item(@item) }

string         : /"(([^"]*)(\\")?)*"/ { term_item(@item) }

# not used , skipping comments instead. need to capture this. 
#        comment        :   /\(\*\s* .*? \s*\*\)/x  { [$item[0], [ $item[1]]]}
#        comment        :   '(*'  /.*/  ')*'   { [$item[0], [ $item[1]]]}

# TODO: complete this list (ie all relational ops, etc.)
# precedence is a little out of order. Lowest prcedence first,
# highest precedence last.
# ? make bin_item() the default action, or leave it explicit.
# Rationalize naming

# <leftop> directive wont allow to insert <commit> directive so that
# <error?> works.  Could change them to explicit rules

# these strip comments
        startrule :    <skip: qr{\s* (\([*] .*? [*]\) \s*)*}xs>  compound
        expr     :     <skip: qr{\s* (\([*] .*? [*]\) \s*)*}xs>  compound

#        startrule :     topstatement
#        expr     :      compound

#  Don't know how to capture comments
#        startrule :      comment
#        expr     :       comment
#        comment        :  <skip:''>  /\(\*.+\*\)/    
#                  {  #print ">>>" . Data::Dumper::Dumper(\@item) . "<<<"; 
#                       term_item(@{[$item[0],$item[2]]})}



        topstatement: <skip:''>  /\s*/ compound(s /;?\s+/) /\s*/  { bin_item(@item) }

# the '&' is used to pick up anonymous functions
        compound :     <leftop: logor /([&;])/ logor > /[;&]?/ 
                       { pop(@item) unless $item[$#item] eq '&';  bin_item(@item) }

	logor	:	<leftop: plus_set ('||') plus_set> { bin_item(@item) }

	plus_set:	<leftop: minus_set ('+=')  minus_set > { bin_item(@item) }

	minus_set :      <leftop: div_set ('-=')  div_set > { bin_item(@item) }

# div_set not being found
	div_set :       <leftop: mult_set (/\/\=/)  mult_set > { # print Data::Dumper::Dumper(\@item); print "text '$text'\n";
                                  bin_item(@item) }

	mult_set:	<leftop: set ('*=')  set > { bin_item(@item) }

	set	:	<leftop: setdelay ('=') setdelay > { bin_item(@item) }

	setdelay :	<leftop: conj (':=') conj> { bin_item(@item) }
	
	conj	:	<leftop: bitand ('&&') bitand > { bin_item(@item) }

	bitand	:	<leftop: equal ('&') equal > { bin_item(@item) }

	equal :	        <leftop: notequal ('==') notequal > { bin_item(@item) }

	notequal :      <leftop: less_equal ('!=') less_equal > { bin_item(@item) }

	less_equal :	<leftop: greater_equal ('<=') greater_equal> { bin_item(@item) }

	greater_equal :	<leftop: greater ('>=') greater> { bin_item(@item) }

	greater :	<leftop: less ('>') less> { bin_item(@item) }

	less  : 	<leftop: plus ('<') plus> { bin_item(@item) }
	
	plus	:	<leftop: minus ('+') minus > { bin_item(@item) }

	minus	:	<leftop: mult ('-') mult > { bin_item(@item) }

	mult	:	<leftop: div ('*') div > { bin_item(@item) }

	div	:	<leftop: dot  ('/') dot > { bin_item(@item) }

	dot	:	<leftop: expo ('.') expo > { bin_item(@item) }

	expo	:	<rightop: incpre ('^') incpre >  {bin_item(@item)}


# moved this to translator
#       part   :      incpost double_square_bracket_expr(s?) { post_item3(@item) }

        incpre :      incop(s?) decpre  { pre_item(@item) }

        decpre :      decop(s?) incpost  { pre_item(@item) }

        incpost :     decpost incop(s?)  {   post_item2(@item) }

        decpost :     unary_plus decop(s?)  { post_item2(@item) }

        incop : '++'
        decop : '--'
        minop : '-'
        plusop : '+'
 
# unary + and - 
 
        unary_plus :      plusop(s?) unary_minus  { pre_item(@item) }
 
        unary_minus :      minop(s?) headblock  { pre_item(@item) }


        headblock   :    unit square_bracket_block(s?)  { post_item(@item) }


        unit	: double_colon_identifier { unit_item(@item) }
                |       farg  { unit_item(@item) } # before identifier
                |       identifier  { unit_item(@item) }
                |       float  { unit_item(@item) }
                |       integer  { unit_item(@item) }
                |       patvar  { unit_item(@item) }
                |       string  { unit_item(@item) }
                |       parentheses_group   { unit_item(@item) }
                |       square_bracket_block  { unit_item(@item) }
                |       curly_brace_list  { unit_item(@item) }



};

our $MmaParser = undef;

sub initialize {
    $MmaParser = new Parse::RecDescent ($MmaGrammar) or 
      die "MmaGrammar is an incorrect grammar specification.";
}

=pod

The routines parse_string and parse_file return raw trees that
need to be processed by MmaToMaxima::parsed_node_to_mma_node,
to finish semantic interpretation.

=cut

# this may return undef if parsing fails
# $string -- string of Mma code
# returns tree
sub parse_string {
    my ($string) = @_;
#    print "GOT STRING $string\n";
    initialize() unless defined $MmaParser;
    $MmaParser->startrule($string);
}


sub parse_file {
    my ($fname) = @_;
    my $HAND;
    open( $HAND , "<$fname") or croak "Can't open Mma code file '$fname' for reading";
    my @lines = <$HAND>;
    close($HAND);
    my $text = (join('',@lines));
    parse_string($text);
}

$Data::Dumper::Terse = 1;

# writes the raw parsed tree
sub write_tree_to_file {
    my ($fname,$tree) = @_;
    print Dumper($tree);
}

1;
