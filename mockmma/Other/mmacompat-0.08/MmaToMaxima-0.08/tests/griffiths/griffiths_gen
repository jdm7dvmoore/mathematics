$Test_name = "Griffiths QI code";
$Top_comment =  "This is some of R Griffiths QI code. This is a real world application\n".
    " that will also test the translator";

@mma_codes =  (
 {

   mma_code => <<'EOSTRING'

adjoint[mat_]:=transpose[Conjugate[mat]];

adjointc[mat_]:= ComplexExpand[Conjugate[transpose[mat]]];

adjointr[mat_]:=transpose[mat];

(*basbell -> following bellbas*)
(*bassbell -> following sbellbas*)

		(*Bell basis, Version 1*)
bell[0] = {1,0,0,1}/Sqrt[2]; 
bell[1] = {0,1,1,0}/Sqrt[2];
bell[2] = {1,0,0,-1}/Sqrt[2]; 
bell[3] = {0,1,-1,0}/Sqrt[2];

		(*Bell basis, Version 2*)
bell[0] = {1,0,0,1}/Sqrt[2]; 
bell[1] = {1,0,0,-1}/Sqrt[2]; 
bell[2] = {0,1,1,0}/Sqrt[2];
bell[3] = {0,1,-1,0}/Sqrt[2];

EOSTRING
},

 {
   mma_code => 'bin2ket[ls_]:= Module[{ket,ln=Length[ls],m},
               ket=Table[0,{2^ln}];
               m=1+Fold[2*#1+#2&,0,ls];
               ++ket[[m]]; ket]',

   comment  => '',
 
   maxima_tests =>  [ { max_tcode => 'bin2ket([0,1,1,0]);', 
                        result => '[0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0];'} ]},

{
mma_code => '
bellbas=transpose[Table[bell[j],{j,0,3}]]; (* bellbas . bellket = ket *) 
basbell=adjoint[bellbas]; (* basbell . ket = ket in Bell basis *)
bell2mat[mat_]:= bellbas . mat . basbell;
',
comment => " *** NOT CORRECT Translator should remember that bell[j] is an array."
},
{
mma_code => '

cgate[w_]:= tenprod[{{1,0},{0,0}},IdentityMatrix[Length[w]]] + 
 tenprod[{{0,0},{0,1}},w];

cnot = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 0, 1}, {0, 0, 1, 0}};

coeffs[v_,b_]:= Conjugate[b] . v ;

copygate[gate_,nn_]:=Module[{fgate=gate,jn},
For[jn=2,jn<=nn,++jn, fgate = tenprod[fgate,gate] ];fgate];(*END copygate*)

cphase = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, -1}};

'
},

{
mma_code => '
 bket[bin_,n_]:=Module[{lst=Table[0,{2^n}],m},
   m=1+Fold[2*#1+#2&,0,
    Take[ToExpression[   Characters[ ToString[2*10^n+bin] ]   ],-n]];
   lst[[m]]=1;lst]; (*END bket*) '
},

{
mma_code => '
blochket[ls_]:= Module[{theta,phi,x=ls[[1]],y=ls[[2]],z=ls[[3]]},
 theta = ArcCos[z];
 If[ ((0==x)&&(0==y))||(0.==x)&&(0.==y), phi=0, phi = ArcTan[x,y],
 phi = ArcTan[x,y]];
 {Cos[theta/2], Sin[theta/2]*E^(I*phi)}];',

  comment => 'NOT CORRECT! where are the parentheses?'
},

#

{ mma_code => '
	diags[mat_] := Module[{j,v=Table[0,{l=Length[mat]}]},
         v=Table[0,{l}]; For[j=1,j<=l,++j, v[[j]] = mat[[j,j]] ]; v];
         ',

   maxima_tests =>  [ { mma_tcode => 'diags[3*ident[4]];', 
                        result => '[3,3,3,3];'} 
   ]},

{ mma_code => '
      	dyad[a_,b_]:= Outer[Times,a,Conjugate[b]];
       	dyadc[a_,b_]:= Outer[Times,a,ComplexExpand[Conjugate[b]]];
       	dyadr[a_,b_]:= Outer[Times,a,b];
	entang[ket_,dl_]:= Module[{eps = 10^-16,evals,j,rho,rhoa,sum,x},
         rho=dyad[ket,ket];
         rhoa = Chop[ partrace[rho,2,dl]/Tr[rho] ];
          evals = Re[ Eigenvalues[rhoa] ];
          sum=0;
        	For[j=1,j<=dl[[1]],++j,
          x=evals[[j]]; 
         If[x > eps, sum += x*Log[x]];
         	];
         -sum/Log[2] ];
(*END entang*)

entsq[ket_,dl_] := Module[{rho,rhoa},
    rho=dyad[ket,ket];
   rhoa = Chop[ partrace[rho,2,dl]/Tr[rho] ];
   -Log[Re[ Tr[rhoa.rhoa] ]]/Log[2.]  ];
(*END entsq*)

	entropy[list_]:=Module[{j,n=Length[list],p,sum},
         sum=0; 
	For[j=1,j<= n,++j,
        p = list[[j]];
       If[p < 10^-12,Continue[] ];
      sum += p*Log[2,p];
	];
-sum] ;
(*END entropy*)

exchg = { {1,0,0,0},{0,0,1,0},{0,1,0,0},{0,0,0,1} };

expandout[op_,ls_,dl_]:=
   permmat[tenprod[op,IdentityMatrix[Fold[Times,1,dl]/Length[op]]],
     Join[ls,invertlist[Length[dl],Sort[ls]]],dl];

expandout2[op_,ls_,n_]:= 
permmat2[addidents[op,n-Log[2,Length[op]]],Join[ls,invertlist[n,Sort[ls]]]];

fivecode = { 
  bket[00000,5] + bket[10010,5] +bket[01001,5] +bket[10100,5] +
   bket[01010,5] - bket[11011,5] -bket[00110,5] -bket[11000,5]
   -bket[11101,5] -bket[00011,5] -bket[11110,5] -bket[01111,5]
   -bket[10001,5] -bket[01100,5] -bket[10111,5] +bket[00101,5] ,
   +bket[11111,5] +bket[01101,5] +bket[10110,5] +bket[01011,5] 
   +bket[10101,5] -bket[00100,5] -bket[11001,5] -bket[00111,5] 
   -bket[00010,5] -bket[11100,5] -bket[00001,5] -bket[10000,5] 
   -bket[01110,5] -bket[10011,5] -bket[01000,5] +bket[11010,5]  }/4;

fourierg[n_]:= Table[Exp[2*Pi*I*j*k/n]/Sqrt[n],
{j,0,n-1}, {k,0,n-1}];

grschm[ls_]:=Module[{j,k,ln=Length[ls],ns={},v,w},
   For[j=1,j<=ln,++j,
      v=ls[[j]]; w=v;
	   For[k=1,k<j,++k,
           w = w - ketinner[ns[[k]],v]*ns[[k]];
	 ]; 
          ns=Append[ns,ketnorm[w]]; 
    	]; ns]; (*END grschm*)


grschmr[ls_]:=Module[{j,k,ln=Length[ls],ns={},v,w},
		For[j=1,j<=ln,++j,
     v=ls[[j]]; w=v;
     	For[k=1,k<j,++k,
   w = w - (ns[[k]].v)*ns[[k]];
	]; 
    ns=Append[ns,ketnormr[w]]; 
		]; ns]; (*END grschm*)


'
},

{
mma_code => '

	hgate = { {1,1}, {1,-1} }/Sqrt[2];
       	ident[n_] := IdentityMatrix[n];
	invertlist[n_,l_]:=Complement[Array[#&,{n}],l];

	invperm[perm_]:=Module[{invp,j,ln=Length[perm]},
    invp=Table[0,{ln}]; 
   For[j=1,j<=ln,++j,invp[[ perm[[j]] ]]=j];invp];(*END invperm*)
		
	ketcofs[v_,b_,dl_]:= Map[Flatten,Conjugate[b] . ket2kten[v,dl]];

	ketinner[v_, w_] := adjoint[v].w;

	ketinnerc[v_, w_] := ComplexExpand[ adjoint[v].w ];

'    
},

);


