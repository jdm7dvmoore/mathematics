use 5.010000;
use ExtUtils::MakeMaker;
# See lib/ExtUtils/MakeMaker.pm for details of how to influence
# the contents of the Makefile that is written.
WriteMakefile(
    NAME              => 'MmaToMaxima',
    VERSION_FROM      => 'lib/MmaToMaxima.pm', # finds $VERSION
    PREREQ_PM         => { Clone => 0.29 }, # e.g., Module::Name => 1.1
    EXE_FILES      => [qw(
                  bin/generate_test.pl  bin/mmatomax_test.pl  bin/parsemma.pl
                  bin/mmatomax.pl       bin/mmatreetomax.pl   bin/parsetomaxtree.pl
                         )],
    ($] >= 5.005 ?     ## Add these new keywords supported since 5.005
      (ABSTRACT_FROM  => 'lib/MmaToMaxima.pm', # retrieve abstract from module
       AUTHOR         => 'John Lapeyre') : ()),
);
