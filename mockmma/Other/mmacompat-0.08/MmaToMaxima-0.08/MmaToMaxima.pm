package MmaToMaxima;

=begin copyright

COPYRIGHT AND LICENCE

Copyright (C) 2008, John Lapeyre  All rights reserved.

This program is free software; you can redistribute it
and/or modify it only under the terms of the GNU General
Public License version 2 as published by the Free Software
Foundation.

=cut


# I don't export anything by default. In all the supplied scripts,
# the fully qualified symbols are used. The exportable symbols are
# probably not even current

@ISA = qw(Exporter);
@EXPORT_OK = qw(read_tree_from_file parsed_node_to_mma_node
                mma_node_to_maxima_node maxima_node_to_maxima_code
                tree_to_maxima_code
		translate_tree translate_file 
    );

use strict;
use warnings;
use Carp ;
use Data::Dumper;           
use MmaToMaxima::ParseMma;  # use the parser module


our $VERSION = '0.08';

=begin comment


Conventions:

* The node argument is first in the arg list of all functions
that take muliple args.

* Use croak instead of die, so that the stack unwinds on printing errors.

* Use accessor functions instead of accessing arrays directly.


=end comment

=cut


# debugging
sub deb { print STDERR  $_[0],"\n"}
#sub deb { }

######### Arrays holding translation details
our @Constant_types = qw(
			 integer
			 float
			 string
			 );

# Ops in this list are translated with a common routine
# I think they are all nary.
our @Infix_op_types = qw (
			   conj
			   bitand
			   set
			   setdelay
			   equal
			   notequal
			   dot
			   exp
			   less_equal
			   logor
			   greater_equal
			   less
			   greater
                           plus
			   minus
			   mult
			   div
			   expo
			   );



our @Prefix_op_types = qw(
			  unary_plus
			  unary_minus
			  );

our @Postfix_op_types = qw(

			  );


our %Conditional_node_type_symbols = (
				 less_equal => '<=',
				 greater_equal => '>=',
				 equal => '=',
				 notequal => '#',
				 less => '<',
				 greater => '>',
				 );

# these symbols are +=, *=, etc in Mma.
# In maxima, we just use the op and a plain old set
our %Op_and_set_node_type_symbols =  (
				      plus_set => '+',
				      minus_set => '-',
				      mult_set => '*',
				      div_set  => '/'
				      );



# maxima symbols
our %Node_type_symbols = (
			  minus => '-',
			  plus => '+',
			  unary_minus => '-',
			  unary_plus => '+',
			  mult => '*',
			  div => '/',
			  set => ':',
			  setdelay => ':=',
			  bitand => '&',
			  conj => 'and',
			  logor => 'or',
			  expo => '^',
			  dot => '.',
                          %Conditional_node_type_symbols,
			  %Op_and_set_node_type_symbols,
			  );


######### For translating special identifiers
our %Special_identifiers_mma_to_max = (
				       'E' => '%e',
				       'I' => '%i'
				       );


# translate identifier or fall through
sub translate_special_identifiers_mma_to_max {
    my ($id) = @_;
    check_terminal_value($id);
    if (exists $Special_identifiers_mma_to_max{$id}) {
	$Special_identifiers_mma_to_max{$id};
    }
    else {
	undef;
    }
}

######### end For translating special identifiers

######### For translating function names
our %Function_names_mma_to_max = (
			     'Module' => 'block',
			     'Block' => 'block'
			       );

sub translate_function_names_mma_to_max {
    my ($name) = @_;
    check_terminal_value($name);
    if (exists $Function_names_mma_to_max{$name} ) {
	$Function_names_mma_to_max{$name};
    }
    else {
	undef;
    }
}

# check if $node is terminal and if it is on the list of function
# names to replace. If so, change the name in the tree
sub translate_function_names_mma_to_max_if_terminal_node {
    my ($node) = @_;
    if ( is_terminal_node($node) ) {
	my $val = 
	    translate_function_names_mma_to_max(get_terminal_node_value($node));
	set_terminal_node_value($node,$val) if defined $val;
    }
}

######### end translating function names


######### For translating functions into Maxima
our %Mma_to_Maxima_Function_handlers;

sub set_maxima_translate_functions_handler {
    my ($function_name,$code) = @_;
    $Mma_to_Maxima_Function_handlers{$function_name} = $code;
}

# This $node will be a 'headblock' node
# If no handler is set for translating this function, return undef, and
# the node is left untranslated.
sub translate_functions_mma_to_max {
    my ($node) = @_;
#    deb "Checking for function translation handlers";
    my $type = get_node_type($node);
    if ( not $type eq 'headblock' ) {
	warn "Expecting a headblock node: got type " . $type;
    }
    my $name = get_headblock_head_identifier($node);
    return undef unless defined $name;
    if (exists 	$Mma_to_Maxima_Function_handlers{$name}) {
	$Mma_to_Maxima_Function_handlers{$name}->($node);
    }
    else {
	undef; # currently not examined 
    }
}



######### end translating function names


######### Maxima type handler manipulation routines (not the handlers!)
our %Maxima_type_handlers;

sub set_maxima_node_type_handler {
    my ($type,$code) = @_;
    check_type($type);
    $Maxima_type_handlers{$type} = $code;
}

sub get_maxima_node_type_handler {
    my ($type) = @_;
    check_type($type);
    if (exists $Maxima_type_handlers{$type}) {
	$Maxima_type_handlers{$type};
    }
    else {
	undef; # this value is used below
    }
}
######### End Maxima type handler manipulation routines 

######### Mma node to Maxima node type handler manipulation routines 
our %Mma_node_to_maxima_node_type_handlers;

sub set_mma_node_to_maxima_node_type_handler {
    my ($type,$code) = @_;
    check_type($type);
#    deb "Setting mma_node_to_maxima_node_type_handler for '$type'";
    $Mma_node_to_maxima_node_type_handlers{$type} = $code;
}

sub  get_mma_node_to_maxima_node_type_handler {
    my ($type) = @_;
    check_type($type);
#    deb "Getting mma_node_to_maxima_node_type_handler for '$type'";
    if (exists $Mma_node_to_maxima_node_type_handlers{$type}) {
#	deb " Found mma_node_to_maxima_node_type_handler for '$type'";
	$Mma_node_to_maxima_node_type_handlers{$type};
    }
    else {
#	deb " Cant find  mma_node_to_maxima_node_type_handler for '$type'";
	undef; # this value is used below
    }
}
######### End  Mma node to Maxima node type handler manipulation routines 

######### translation of Mma function to  Maxima function handler manipulation routines 



######### Parsed node to Mma node type handler manipulation routines 
our %Parsed_node_to_mma_node_type_handlers;

sub set_parsed_node_to_mma_node_type_handler {
    my ($type,$code) = @_;
    check_type($type);
    $Parsed_node_to_mma_node_type_handlers{$type} = $code;
}

sub  get_parsed_node_to_mma_node_type_handler {
    my ($type) = @_;
    check_type($type);
    if (exists $Parsed_node_to_mma_node_type_handlers{$type}) {
	$Parsed_node_to_mma_node_type_handlers{$type};
    }
    else {
	undef; # this value is used below
    }
}
######### Parsed node to Mma node type handler manipulation routines 


######### Translation option routines
our %Trans_options;

sub set_translation_options_defaults {
    $Trans_options{infix_op_print_style} = 'space';
}

set_translation_options_defaults();

sub get_trans_option {
    my ($option) = @_;
    if ( exists  $Trans_options{$option} ) {
	$Trans_options{$option};
    }
    else {
	croak("get_trans_option: Unknown option '$option'");
    }
}
######### End Translation option routines

############################
=begin comment

 Accessor functions. These functions know the
 implementationf of nodes. If possible, all functions That
 directly access the arrays implementing the nodes should be
 here. In particular, the node type (scalar) ought to
 eventually be replaced by an array with more information.

=cut

# crude checks, but very useful for coding this API.
# They should be improved
sub check_node {
    my ($node) = @_;
    croak "node is an undefined perl variable" unless defined $node;
    croak "node '$node' not a ref" unless ref($node);
}

sub check_type {
    my ($type) = @_;
    croak "type not a scalar" if ref($type);
}

sub check_body {
    my ($body) = @_;
    croak "body not a ref" unless ref($body);
}

sub check_terminal_value {
    my ($val) = @_;
    croak "terminal value (identifier,integer,string,float,...) not a scalar"
	if ref($val);
}

# return string telling what type of node.
sub get_node_type {
    my ($node) = @_;
    check_node($node);
    my $type = $node->[0];
    check_type($type);
    $type;
}

sub set_node_type {
    my ($node,$type) = @_;
    $node->[0] = $type;
}

sub new_node {
    my ($type) = @_;
    $type = '' unless defined $type;
    [$type,[]];
}

sub create_node {
    my ($type,@body) = @_;
    [$type,[@body]];
}

sub new_terminal_node {
    my ($type,$val) = @_;
    [$type,[$val]];
}


sub get_node_body {
    my ($node) = @_;
    my $body = $node->[1];
    check_body($body);
    $body;
}

sub set_node_body {
    my ($node,$body) = @_;
    $node->[1] = $body;
}


# length of body of node
sub get_node_length {
    my ($node) = @_;
    check_node($node);
    my $body = get_node_body($node);
    check_body($body);
    scalar(@$body);
}


# takes terminal node body and returns the
# value, ie contents.
sub get_terminal_node_body_value {
    my ($body) = @_;
    $body->[0];
}

sub set_terminal_node_body_value {
    my ($body,$val) = @_;
    $body->[0] = $val;
}

# Get a deep subpart part of the body of a node,
# specified by indices.
# This is another node.
# First part of body is part 1.
# Part zero is the type.
# 
sub get_node_part {
    my ($node,@indices) = @_;
    my $p = get_node_body($node);
    foreach (@indices) {
	$p = $p->[($_) - 1 ];
    }
    $p;
}

# get first part of node
sub get_node_first {
    my ($node) = @_;
    my $p = get_node_body($node);
    $p->[0];
}

# currently only works on top level.
sub get_node_parts {
    my ($node,$i1,$i2) = @_;
    my $p = get_node_body($node);
    $i1--;
    $i2--;
    @{$p}[$i1..$i2];
}

# same but set part
sub set_node_part {
    my ($node,@indices,$subnode) = @_;
    my $p = get_node_body($node);
    foreach (@indices) {
	$p = $p->[($_) - 1 ];
    }
    $p = $subnode;
}



# The body of a node consists of one or more nodes.
# Map a function over these nodes and return the resulting
# list. This function takes the whole *node* as an arg.
sub map_over_node_body {
    my ($node,$code_block) = @_;
    my $body = get_node_body($node);
    map({$code_block->($_)}  @$body);
}


# end Accessor functions.
############################

#### These are also 'accessor' functions, but they do not
# know the array structure of the nodes.

sub get_terminal_node_value {
    my ($node) = @_;
    get_terminal_node_body_value(get_node_body($node));
}

sub set_terminal_node_value {
    my ($node,$val) = @_;
    set_terminal_node_body_value(get_node_body($node),$val);
}


# get the head node of a headblock
sub get_headblock_head {
    my ($node) = @_;
#    deb "HEADBLOCK_HEAD " . Dumper($node);
    my $type = get_node_type($node);
    if ( not ( $type eq 'headblock' or $type eq 'array_headblock'  )) {
	warn "Node not a headblock: type  " . get_node_type($node);
    }
    get_node_part($node,1);
}

# get the head node of a headblock
sub get_headblock_body {
    my ($node) = @_;
    my $type = get_node_type($node);
    croak "Node not a headblock" unless $type eq 'headblock'
        or $type eq 'array_headblock';
    get_node_part($node,2);
}


# Assuming a head block or similar
# structure, get the identifier of the
# head, or undef if head is not an identifier.
# Argument is the node, not the head of the node
sub get_headblock_head_identifier {
    my ($node) = @_;
    my $head_node = get_headblock_head($node);
    if ( is_terminal_node($head_node) ) {
	get_terminal_node_value($head_node);
    }
    else {
	undef;
    }
}

# Get the Maxima symbol associate with the operator specified by
# the node type
sub get_node_type_symbol {
    my ($node) = @_;
    my $type = get_node_type($node);
    if (exists $Node_type_symbols{$type} ) {
	$Node_type_symbols{$type};
    }
    else
    {
	croak "No symbol for type '$type'";
    }
}

sub get_conditional_node_type_symbol {
    my ($node) = @_;
    my $type = get_node_type($node);
    if (exists $Conditional_node_type_symbols{$type} ) {
	$Node_type_symbols{$type};
    }
    else
    {
	croak "No symbol for type '$type'";
    }
}

sub get_op_and_set_node_type_symbol {
    my ($node) = @_;
    my $type = get_node_type($node);
    if (exists $Op_and_set_node_type_symbols{$type} ) {
	$Node_type_symbols{$type};
    }
    else
    {
	croak "No symbol for type '$type'";
    }
}


####  query routines ###
# is the node terminal
sub is_terminal_node {
    my ($node) = @_;
    check_node($node);
    my $length = get_node_length($node);
    return 1 if get_node_length($node) == 1
	and not ref(get_node_part($node,1));
#    deb "is_terminal_node: node type " . get_node_type($node) . " not terminal ";
    undef;
}


# test if node type is $type
# return 1 or undef
sub is_node_type {
    my $nargs = scalar(@_);
    croak "is_node_type needs two args, got $nargs" unless $nargs == 2;
    my ($node,$type) = @_;
    return 1 if get_node_type($node) eq $type;
    undef;
}

sub is_type_in_list {
   my ($node,$slist) = @_;
   my $type = get_node_type($node);
   foreach (@$slist) {
       return 1 if $type eq $_;
   }
   return undef;
}

sub is_type_in_hash {
    my ($node,$thash) = @_;
    return undef unless  exists $thash->{get_node_type($node)};
    1;
}

# Is the value of the terminal node one of the known constant
# types ? ie, integer,etc. (as opposed to eg an identifier)
sub is_node_type_constant {
    my ($node) = @_;
    is_type_in_list($node,\@Constant_types);
}

sub is_node_type_infix_op {
    my ($node) = @_;
    is_type_in_list($node,\@Infix_op_types);
}

sub is_node_type_prefix_op {
    my ($node) = @_;
    is_type_in_list($node,\@Prefix_op_types);
}

sub is_node_type_postfix_op {
    my ($node) = @_;
    is_type_in_list($node,\@Postfix_op_types);
}

sub is_node_type_conditional {
    my ($node) = @_;
    is_type_in_hash($node,\%Conditional_node_type_symbols);
}

sub is_node_type_op_and_set {
    my ($node) = @_;
    is_type_in_hash($node,\%Op_and_set_node_type_symbols);
}

sub are_terminal_node_strings_equal {
    my($node1,$node2) = @_;
    get_terminal_node_value($node1) eq 
	get_terminal_node_value($node2);
}

#### end query routines ###

############### Diagnostic dumping functions #####################

# Diagnostic.  Return a node (or tree) converted to a 'FullForm'
# string.
sub map_fullform_string {
    my ($node) = @_;
    if ( is_terminal_node($node) ) {
	return get_node_type($node) . "['"
	    . get_terminal_node_value($node) . "']"
    }
    else {
	get_node_type($node) . "["
	. join( ',', map_over_node_body($node,\&map_fullform_string))
	. "]"
    }
}

# Diagnostic. Print node types, lengths, and values (if terminal)
# recursively of node.
sub map_print_node_types_and_lengths {
    my ($node) = @_;
    if ( is_terminal_node($node) ) {
	print STDERR get_node_length($node) . " "
	    . get_node_type($node) . " '" .
         get_terminal_node_value($node). "'\n";
     return;
    }
    print STDERR get_node_length($node) . " "
	. get_node_type($node) . "\n";
    map_over_node_body($node,\&map_print_node_types_and_lengths);
}


# This is broken. Will take a bit of work
sub map_lispform_string {
    my ($node) = @_;
    if ( is_terminal_node($node) ) {
#	return '(' .get_node_type($node) 
#	    . " '" . get_terminal_node_value($node) . ')'
	my $rstring = get_terminal_node_value($node);
	$rstring = "'" . $rstring  unless is_constant($node);
	return ' '.$rstring;
    }
    else {
	my $body = get_node_body($node);
	my $type = get_node_type($node);
	'( ' . join( ' ', map(  { map_lispform_string($_) }  @$body ));
    }
}

############### End Diagnostic dumping functions #####################

############################################################### 
# maxima node to maxima code routines
############################################################### 

###  MAIN translation node #####
sub maxima_node_to_maxima_code {
    my ($node) = @_;
#    print STDERR "ENTERING PRINT NODE: " . Dumper($node);    
    my $type = get_node_type($node);
#    deb "translating node type '$type'";
    my $typecode = find_maxima_node_handler($node);
    if ( defined $typecode ) {
	$typecode->($node);
    }
    else {
#	deb "Using stub for $type"; #stub when there is no translation
	"<$type>[" . map_maxima_node_to_maxima_code($node) . "]";
    }
}

# print expressions with comma separator.
# with space
sub map_maxima_node_to_maxima_code {
    my ($node) = @_;
    map_maxima_node_to_maxima_code_sep($node,', ');
}

# print expressions with supplied separator
# Note that this stringifies the resuling list !
sub map_maxima_node_to_maxima_code_sep {
    my ($node,$sep) = @_;
    if ( is_terminal_node($node) ) {
	get_terminal_node_value($node);
    }
    else {
	join($sep, map_over_node_body($node,\&maxima_node_to_maxima_code));
    }
}

# Note, 'infix' ops are actually nary
# This finds some handlers that work for more than one op
# then falls through to individual handlers.
sub  find_maxima_node_handler {
    my ($node) = @_;
    my $type = get_node_type($node);
    if ( is_node_type_infix_op($node) ) {
	get_maxima_node_type_handler('infix_op');
    }
    elsif ( is_node_type_constant($node) ) {
	get_maxima_node_type_handler('constant');
    }
    elsif ( is_node_type_prefix_op($node) ) {
	get_maxima_node_type_handler('prefix_op');
    }
    elsif ( is_node_type_postfix_op($node) ) {
	get_maxima_node_type_handler('postfix_op');
    }
    elsif ( is_node_type_op_and_set($node) ) {
	get_maxima_node_type_handler('op_and_set');
    }
    else {
	get_maxima_node_type_handler($type);
    }
}

set_maxima_node_type_handler('incpre',
    sub {
	my ($node) = @_;
	my $s = maxima_node_to_maxima_code(get_node_part($node,1));
	"(($s):($s)+1)";
    });

set_maxima_node_type_handler('decpre',
    sub {
	my ($node) = @_;
	my $s = maxima_node_to_maxima_code(get_node_part($node,1));
	"(($s):($s)-1)";
    });

set_maxima_node_type_handler('incpost',
    sub {
	my ($node) = @_;
	my $s = maxima_node_to_maxima_code(get_node_part($node,1));
	"((($s):($s)+1,($s)-1))";
    });

set_maxima_node_type_handler('decpost',
    sub {
	my ($node) = @_;
	my $s = maxima_node_to_maxima_code(get_node_part($node,1));
	"((($s):($s)+1,($s)-1))";
    });

set_maxima_node_type_handler('op_and_set',
  sub {
      my ($node) = @_;
      my $sym = get_op_and_set_node_type_symbol($node);
      my $lhs = maxima_node_to_maxima_code(get_node_first($node));
      my $rhs = maxima_node_to_maxima_code(get_node_part($node,2));
      "$lhs : $lhs $sym $rhs";
  });

set_maxima_node_type_handler('prefix_op',
    sub {
	my ($node) = @_;
	my $sym = get_node_type_symbol($node);
	$sym . maxima_node_to_maxima_code(get_node_part($node,1));
    });

set_maxima_node_type_handler('prefix_op',
    sub {
	my ($node) = @_;
	my $sym = get_node_type_symbol($node);
	$sym . maxima_node_to_maxima_code(get_node_part($node,1));
    });

set_maxima_node_type_handler('postfix_op',
    sub {
	my ($node) = @_;
	my $sym = get_node_type_symbol($node);
	maxima_node_to_maxima_code(get_node_part($node,1)) . $sym;
    });


# note the use of map because these are mostly nary
set_maxima_node_type_handler('infix_op',
    sub {
	my ($node) = @_;
	my $sym = get_node_type_symbol($node);
	if ( get_trans_option('infix_op_print_style') eq 'space' ) {
        	$sym = ' ' . $sym . ' ';
        }
	map_maxima_node_to_maxima_code_sep($node,$sym);
    });


set_maxima_node_type_handler('compound',
 sub {
     my ($node) = @_;
     map_maxima_node_to_maxima_code_sep($node,",\n");
  });			     



# These nodes have two elements a head and body (a block)
set_maxima_node_type_handler('headblock',
sub {
	my ($node) = @_;
	my $head = get_headblock_head($node); # part 1
	my $body = get_headblock_body($node);    # part 2
	maxima_node_to_maxima_code($head) . '('
	    . 	map_maxima_node_to_maxima_code($body)
	    . ')';
});


# These nodes have two elements a head and body (a block)
set_maxima_node_type_handler('array_headblock',
sub {
	my ($node) = @_;
	my $head = get_headblock_head($node); # part 1
	my $body = get_headblock_body($node);    # part 2
	maxima_node_to_maxima_code($head) . '['
	    . 	map_maxima_node_to_maxima_code($body)
	    . ']';
});



# curly braces become square brackets
set_maxima_node_type_handler('curly_brace_list',
    sub {
	my ($node) = @_;
	'[' . map_maxima_node_to_maxima_code($node) . ']'
    });

set_maxima_node_type_handler('parentheses_group',
    sub {
	my ($node) = @_;
	'(' . map_maxima_node_to_maxima_code($node) . ')'
    });


set_maxima_node_type_handler('partblock',
   sub {
       my ($node) = @_;
       my $head = get_node_part($node,1);
#       maxima_node_to_maxima_code($head) . 
       my $specst = '';
       for(my $i=2;$i<=get_node_length($node);$i++) {
	   my $partspecs = get_node_part($node,$i);
	   $specst .= join('', map_over_node_body($partspecs, sub {'[' . maxima_node_to_maxima_code($_[0]) . ']'}))
	   }
       maxima_node_to_maxima_code($head) . $specst;
   });


set_maxima_node_type_handler('identifier',
    sub {
	my ($node) = @_;
	get_terminal_node_value($node);
    });

# strip trailing underscores for function arguments
set_maxima_node_type_handler('farg',
    sub {
	my ($node) = @_;
	my $val = get_terminal_node_value($node);
	$val =~ s/(_)+$//;
	$val;
    });


set_maxima_node_type_handler('constant',
    sub {
	my ($node) = @_;
	get_terminal_node_value($node);
    });

# pattern variables. need to find the correct mma name for these.
# Just change the '#' into arg, '##' into argarg, etc.
set_maxima_node_type_handler('patvar',
    sub {
	my ($node) = @_;
	my $val = get_terminal_node_value($node);
	$val =~ s/\#/arg/g;
	$val;
    });


# square bracket block has no delimeters, they are printed
# at a higher level
set_maxima_node_type_handler('square_bracket_block',
    sub {
	my ($node) = @_;
	map_maxima_node_to_maxima_code($node);
    });


####### Lamba function handler routines

# The following two descend into the lambda function and
# record names of pattern variables.
sub find_pattern_args  {
    my ($node) = @_;
    my %pats;
    _find_pattern_args($node,\%pats);
    my @pats = keys(%pats);
    return(@pats);
}

# the ++ is a perl idiom to return a list of unique items,
# see, keys in previous function
sub _find_pattern_args {
    my ($node,$pats) = @_;
    if ( get_node_type($node) eq 'patvar' ) {
	$pats->{get_terminal_node_value($node)}++;
    }
    elsif ( is_terminal_node($node) ) {
	return;
    }
    else {
	map_over_node_body($node,sub{_find_pattern_args($_[0],$pats)});
    }
}

# The pattern argument translation should be done in mma node to maxima node stage.
# below, get_node_part($node,1)  is the body of the lambda function
# This must be moved to the mma node to max node code
set_maxima_node_type_handler('lambda',
   sub {
       my ($node) = @_;
       my @pats = find_pattern_args($node);
       foreach(@pats) {
	   s/\#/arg/g;
       }
       my $argst = join(',',@pats);
       "lambda([$argst]," .  maxima_node_to_maxima_code(get_node_part($node,1)) . ")";
   });


set_maxima_node_type_handler('for_while',
  sub {
      my ($node) = @_;
      my ($init,$step,$cond,$statement) = get_node_parts($node,1,4);
   ' for ' . maxima_node_to_maxima_code($init) . ' '
     . ' step ' . maxima_node_to_maxima_code($step) . ' while '
        . maxima_node_to_maxima_code($cond) . ' '
        . ' do ( ' . maxima_node_to_maxima_code($statement) . ')';
  });			     

####### end Lamba function handler routines

############################################################### 
# end maxima node to maxima code routines
############################################################### 


############################################################### 
# parsed node to mma code routines
############################################################### 

sub parsed_node_to_mma_node {
    my ($node) = @_;
#    deb "Dumping node\n" . Dumper($node);
    my $typecode = get_parsed_node_to_mma_node_type_handler(get_node_type($node));
    if ( defined $typecode ) {
	$typecode->($node);
    }
    elsif ( is_terminal_node($node)) {

    }
    else {
	map_over_node_body($node,\&parsed_node_to_mma_node);
    }    
}

# check for nested square brackets, which are actually the part operator
# I think this makes a little memory leak
set_parsed_node_to_mma_node_type_handler('square_bracket_block',
  sub {
      my ($node) = @_;
      if (get_node_length($node) == 1 and
	  get_node_type(get_node_part($node,1)) eq 'square_bracket_block') {
	  set_node_type($node, 'double_square_bracket_block');
	  set_node_body($node, get_node_body(get_node_part($node,1)));
      }
      map_over_node_body($node,\&parsed_node_to_mma_node);
  });


############################################################### 
# end  parsed node to mma code routines
############################################################### 

############################################################### 
# mma node to maxima node routines
############################################################### 

=begin comment

Something ...

=cut

sub mma_node_to_maxima_node {
    my ($node) = @_;
    my $type = get_node_type($node);
#    deb "Checking typecode '$type'";
    my $typecode = get_mma_node_to_maxima_node_type_handler($type);
    if ( defined $typecode ) {
	$typecode->($node);
    }
    elsif ( is_terminal_node($node) ) {
#	deb "mma_node_to_maxima_node: got terminal node '$type'";
	undef;
    }
    else {
#	deb "Mapping over type '$type'";
	map_over_node_body($node,\&mma_node_to_maxima_node);
    }
}

# this is a terminal node handler
set_mma_node_to_maxima_node_type_handler('identifier',
   sub {
      my ($node) = @_;
#      deb "In mma_node_to_maxima_node_type_handler 'identifier'";
      my $id = get_terminal_node_value($node);
      my $val = translate_special_identifiers_mma_to_max($id);
      if (defined $val) {
	  set_terminal_node_value($node,$val);
      }
});				 

=begin comment

 In the maxima tree, we continue to call an argument list a
 square_bracket_block. I guess I could rename them all.  But
 if mma has a double square bracket part specification, we
 change the type to 'partblock'

=cut

set_mma_node_to_maxima_node_type_handler('headblock',
   sub {
      my ($node) = @_;
      for (my $i = 2; $i<=get_node_length($node); $i++) {
	  my $block = get_node_part($node,$i);
	  if (get_node_type($block) eq 'double_square_bracket_block') {
	      set_node_type($node,'partblock');
	      set_node_type($block,'square_bracket_block');
	  }
      }
# remember , part 0 is the 'type'. Maybe this is a confusing difference between
# this scheme and lisp/maxima
      if ( get_node_type($node) eq 'headblock' ) { # it may have been changed above to partblock
	  my $head = get_headblock_head($node);
	  translate_function_names_mma_to_max_if_terminal_node($head); # just change name
	  translate_functions_mma_to_max($node); # rewrite the entire node into Maxima
      }
      map_over_node_body($node,\&mma_node_to_maxima_node);
});				 


# Convert double colons to underscores in identifiers
set_mma_node_to_maxima_node_type_handler('double_colon_identifier',
   sub {
      my ($node) = @_;
      set_node_type($node,'identifier');
      my $val = get_terminal_node_value($node);
      $val =~ s/\:\:/_/g;
      set_terminal_node_value($node,$val);
  });

# assume that lhs of 'set' (as opposed to setdelay) is
# actually an array element if it is a headblock.
set_mma_node_to_maxima_node_type_handler('set',
   sub {
       my ($node) = @_;
       my $lhs = get_node_part($node,1);
       my $type =  get_node_type($lhs);
       my $typecode = get_mma_node_to_maxima_node_type_handler($type);
       if ( defined $typecode ) {
	   $typecode->($lhs);
       }
       $type =  get_node_type($lhs); # get type again in case it changed
       if ( $type eq 'headblock' ) {
	   set_node_type($lhs,'array_headblock');
       }
       map_over_node_body($node,\&mma_node_to_maxima_node);
   });


# Needs cleaning. maybe inefficient at end. maybe more error checking
set_maxima_translate_functions_handler( 'For',
  sub {
      my ($node) = @_;
      my $nostr = " not translating";
#      deb "Translating For function ";
#      deb "For node dumped\n" . Dumper($node);
      if ( not (is_node_type($node,'headblock'))) {
	  carp "For function not headblock: $nostr";
	  return undef;
      }
#      deb "For: Looking for head";
      my $head = get_headblock_head($node);
#      deb "For: got head";
      my $body = get_headblock_body($node);
#      deb "For: got body";
#      deb "Length of For body (4?) = " . get_node_length($body);
      my $init =     get_node_part($body,1);
      my $cond =     get_node_part($body,2);
      my $incr =     get_node_part($body,3);
      my $statement = get_node_part($body,4);
      if ( get_node_type($init) ne 'set' ) {
	  warn "For loop initialization does not start with assignment: $nostr";
	  return undef;
      }
      my $itvar = get_node_part($init,1); 
      my $initval = get_node_part($init,2); 
#      my $itvar_name = get_terminal_node_value($itvar);
      if ( not is_node_type_conditional($cond) ) {
	  warn "For loop conditional unrecognized: $nostr";
	  return undef;
      }
      my $cond_sym = get_conditional_node_type_symbol($cond); # the maxima symbol
      my $incr_type = get_node_type($incr);
      my $step = 0;
      my $step_node;
      if ( $incr_type eq 'incpre' or $incr_type eq 'incpost') {
	  $step = 1 ;
      }
      elsif ( $incr_type eq 'decpre' or $incr_type eq 'decpost') {
	  $step = -1 ;
      }
      elsif ( $incr_type eq 'plus_set' or $incr_type eq 'minus_set'
	      and is_terminal_node(get_node_first($incr))) {
	  if (are_terminal_node_strings_equal(get_node_first($incr),
					      $itvar) ) {
	      if ( $incr_type eq 'plus_set') {
		  $step_node = get_node_part($incr,2);
	      }
	      else {
		  if ( is_terminal_node(get_node_part($incr,2))) {
		  $step_node = create_node('unary_minus',get_node_part($incr,2))
		  }
		  else {
		      $step_node = create_node('unary_minus',
					       create_node('parentheses_group',get_node_part($incr,2)));
		  }
	      }
	  }
	  else {
	      warn "For loop: increment var different from initializer var: $nostr";
	      return undef;
	  } 
      }
      else {
	  warn "For loop: unimplemented increment statement: $nostr";
	  return undef;
      }
      if ( $step != 0 ) {
	  $step_node = new_terminal_node('integer',$step);
      }
      my $newnode = create_node('for', $init, $step_node, $cond, $statement);
#      deb "NEWNODE" . Dumper($node);
      set_node_type($node,'for_while');
      set_node_body($node,get_node_body($newnode));
      map_over_node_body($node,\&mma_node_to_maxima_node);
  });

=pod


set_maxima_translate_functions_handler( 'If',
  sub {
      my ($node) = @_;
      my $nostr = " not translating";
      if ( not (is_node_type($node,'headblock'))) {
	  carp "For function not headblock: $nostr";
	  return undef;
      }
      if ( get_node_length($node) != 4 ) {
	  croak "Wrong number of arguments in Mma For call";
      }
      my $head = get_headblock_head($node);
      my $body = get_headblock_body($node);
      my $cond =  get_node_part($body,1);
      my $true_statement =     get_node_part($body,2);
      my $false_statement = get_node_part($body,3);
      if ( not is_node_type_conditional($cond) ) {
	  warn "If statment conditional unrecognized: $nostr";
	  return undef;
      }
      my $step_node = new_terminal_node('integer',1);
      my $newnode = create_node('for', $init, $step_node, $cond, $statement);
#      deb "NEWNODE" . Dumper($node);
      set_node_type($node,'for_while');
      set_node_body($node,get_node_body($newnode));
  });

=cut

############################################################### 
# end  mma node to maxima node routines
############################################################### 

###################################
# IO routines
# read raw node from text file. It is in















# the terse form written by Data::Dumper.
# not used, i think
sub read_tree_from_file {
    my ($fname) = @_;
    open IHAND, "<$fname" or croak "Can't read raw parsed tree from file '$fname'";
    my @tree = <IHAND>;
    close(IHAND);
    my $tree = eval(join("",@tree));
    $tree;
}
# end IO routines
###################################

###############################################
# remember a tree is a particular type of node.
# in  : $tree -- raw tree returned from parser
# out : $max_code_string -- maxima code string
# note that two of these functions alter the tree.

sub translate_tree {
    my ($tree) = @_;
#    deb "raw Parsed  tree\n" . Dumper($tree);
    parsed_node_to_mma_node($tree);  
#    deb "Parsed to Mma tree " . Dumper($tree);
    mma_node_to_maxima_node($tree);
#    deb "Mma tree to Max tree " . Dumper($tree);
    my $max_code_string = maxima_node_to_maxima_code($tree);
}

=begin comment

The following are intended to be API functions. But I prefer
not to export them, but use the fully qualified form. Perl does
not prevent you from doing that with any function in the module.

=cut

sub translate_mma_tree_string {
    my ($string) = @_;
    my $tree = eval($string);
    mma_node_to_maxima_node($tree);
    maxima_node_to_maxima_code($tree);
}

# mma code string to processed tree
sub string_to_processed_tree {
    my ($string) = @_;
#    print STDERR "Doing parse";
    my $rawtree = MmaToMaxima::ParseMma::parse_string($string);
#    print STDERR "parser" . Dumper($rawtree);
    if ( not defined $rawtree) {
	carp("Parsing of Mma code string failed");
	return undef;
    }
    parsed_node_to_mma_node($rawtree);
    $rawtree;
}

# mma code string to maxima code tree
sub string_to_maxima_tree {
    my ($string) = @_;
    my $rawtree = MmaToMaxima::ParseMma::parse_string($string);
    if ( not defined $rawtree) {
	carp("Parsing of Mma code string failed");
	return undef;
    }
    parsed_node_to_mma_node($rawtree);
    mma_node_to_maxima_node($rawtree);
    $rawtree;
}

$Data::Dumper::Terse = 1;

sub string_to_maxima_tree_string {
    my ($string) = @_;
    my $tree = string_to_maxima_tree($string);
    Dumper($tree);
}

sub string_to_processed_tree_string {
    my ($string) = @_;
    my $tree = string_to_processed_tree($string);
    Dumper($tree);
}

# translate a file containing mma code
sub translate_mma_file {
    my ($fname) = @_;
    my $rawtree = MmaToMaxima::ParseMma::parse_file($fname);
    if ( not defined $rawtree) {
	carp("Parsing of Mma code string failed");
	return undef;
    }
    my $max_code = translate_tree($rawtree);
    if ( not defined $max_code) {
	carp("Translation of parsed Mma tree failed");
	return undef;
    }
    $max_code;
}

sub translate_mma_string {
    my ($string) = @_;
    my $rawtree = MmaToMaxima::ParseMma::parse_string($string);
    if ( not defined $rawtree) {
	carp("Parsing of Mma code string failed");
	return undef;
    }
    my $max_code = translate_tree($rawtree);
    if ( not defined $max_code) {
	carp("Translation of parsed Mma tree failed");
	return undef;
    }
    $max_code;
}

1;

__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

MmaToMaxima - Translate Mathematica source code to Maxima source code

=head1 SYNOPSIS

  use MmaToMaxima;

=head1 DESCRIPTION

None

=head2 EXPORT

None by default.


=head1 SEE ALSO


If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

John Lapeyre

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2008 by John Lapeyre

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
