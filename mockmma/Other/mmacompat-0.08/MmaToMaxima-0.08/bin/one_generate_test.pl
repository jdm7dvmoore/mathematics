#!/usr/bin/perl

# take test name on std in. with no _gen or rtest_
# and no full path. Eg 'aritmetic'
# Generate the test

my $test = $ARGV[0];

my $gen = "./tests/$test/${test}_gen";
my $rtest = "./tests/$test/rtest_${test}";

sub dosys {
    my ($c) = @_;
    print( $c."\n");
    system($c);
}

dosys "./generate_test.pl $gen > $rtest";
