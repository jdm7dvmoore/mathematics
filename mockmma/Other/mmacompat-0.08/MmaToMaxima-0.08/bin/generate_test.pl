#!/usr/bin/perl
use strict;
use MmaToMaxima;
use Data::Dumper;
use Data::Compare;
use Clone;
use Carp;
$Carp::Verbose = 1;


=pod

  Usage: ./bin/generate_test.pl tests/feature/feature_gen > tests/feature/rtest_feature
  (or use regenerate_tests.pl)

  This generates test specs, but does not run them.

  Generate tests from Mma code. This script parses the
  code and translates it and writes the results to a file in
  a form that can be read by mmatomax_test.pl. mmatomax_test.pl checks
  that the results are the same as when generate_test.pl was run. Ie,
  it may detect errors introduced by changes in the translator since
  the tests were generated.

  The argument to this script is a filename specifiying the tests
  to be generated. The content of the file looks like this:

  @mma_codes = (
		{
                 mma_code =>  'mmacode string', # string has ';' at end
                 comment => 'comment string',
                 maxima_tests => [
                          [ 
                            {  max_tcode =>  'maxima code string;', result => 'result string;' },
                            {  mma_tcode =>  'mma code string;', result => 'result string;' },
                           ... another test ....,
                          ]
                },
                ...  another block of tests
	);

  The outer level string 'mma_code' is some mma code,
  typically a function definition. Three tests are performed
  on the string: parsing, translating, and interpreting the
  result with Maxima. If 'mma_code' is set to undef, ie
  mma_code => undef, then it is not used. The interpretation
  tests only that the maxima code is valid. If element
  'no_interpret' is present , eg ' no_interpret => 1 ' then
  the interpretation is skipped.  This allows checking
  correct parsing that results in illegal Maxima code.

  'maxima_tests' specifies regression tests to be run with
  the Maxima command batch("testfile.mac",test). Each test
  has two entries, a test and a result, as any maxima
  regression test does.  Either 'max_tcode' or 'mma_tcode'
  must be present If 'mma_tcode' is present, the code string
  is translated from mma to maxima before being written into
  the regression test. The 'result' entry must be present
  and is the expected result of executing the code
  ('max_tcode' or 'mma_tcode').  The outer level 'mma_code'
  entry is translated and written into the file before all
  regression tests. In this way the regression tests can
  test features of a function written in 'mma_code'

  All strings representing  code/expressions must  be terminated
  with a semi-colon!

  When running this script, check the results
  carefully. Whatever the results, this script writes a file
  that says they are correct. When you are satisfied that
  they are correct, don't re-run this script, but only run
  mmatomax_test.pl on the results to see that changes in the
  parser and translator have not introduced errors.

=cut


our $Test_name;
our $Top_comment;

our @tests;


$Data::Dumper::Terse = 1;
our @mma_codes;

# load the codes from file name given on stdin

if ( not defined $ARGV[0] ) {
  die "generate_test.pl requires a test specification file as a command line argument";
}

if ( not -e $ARGV[0]) {
  die "Can't find file " .$ARGV[0];
}

require($ARGV[0]);

#print Dumper(\@mma_codes);

our $count = 0;
my $one_mma_code;
my $comment;
my $maxima_tests;
my $parse_tree;
my $parse_tree_copy;
my $translation;

print "\$Test_name = \"$Test_name\";\n\n";
print "\$Top_comment = \"$Top_comment\";\n\n" if defined $Top_comment;

foreach  my $set (@mma_codes) {
    if (exists $set->{mma_code} and defined  $set->{mma_code} 
          and $set->{mma_code} )  {
	$parse_tree = MmaToMaxima::ParseMma::parse_string($set->{mma_code});
        if ( not defined $parse_tree ) {
         die("generate_test.pl: parser failed to parse Mma code: '".
               $set->{mma_code} . "'");
        }
# make clone of tree because the translation may alter it
	$parse_tree_copy = Clone::clone($parse_tree);
#    print STDERR "generate test: " . Dumper($parse_tree);
	$translation = MmaToMaxima::translate_tree($parse_tree);
     if ( not defined $translation ) {
        die("generate_test.pl: translator failed to translate Mma tree");
     }
#    print STDERR $translation ."\n";
    }
    else {
	$set->{mma_code} = undef;
	$translation = undef;
	$parse_tree_copy = undef;
    }
    print "\$tests[$count] = ";
# print the copy
    my $dhash = {
	mma_code => $set->{mma_code},
	translation =>  $translation,
	maxima_tests => $set->{maxima_tests},
	expected_parse_tree => $parse_tree_copy };
    $dhash->{comment} = $set->{comment} if exists $set->{comment};
    $dhash->{no_interpret} = $set->{no_interpret} if exists $set->{no_interpret};
    print Dumper( $dhash);
    print ";\n\n";
    $count++;
}



