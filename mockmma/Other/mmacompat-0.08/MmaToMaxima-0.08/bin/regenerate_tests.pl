#!/usr/bin/perl

@tests = `ls tests/*/*_gen`;

sub dosys {
    my ($c) = @_;
    print( $c."\n");
    system($c);
}

foreach (@tests) {
    chomp;
    my $rtest = $_;
    $rtest =~ s/_gen$//;
    $rtest =~ s#/([^/]+)$#/rtest_$1#;
    dosys("generate_test.pl $_  > $rtest");
}
