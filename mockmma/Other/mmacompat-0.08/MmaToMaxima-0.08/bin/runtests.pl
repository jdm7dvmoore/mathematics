#!/usr/bin/perl

=begin comment

Usage:  ./bin/runtests.pl

Run this from the top level of the distribution.
This script looks for all the tests and runs
mmatomax_test.pl.


=cut

@tests = `ls tests/*/rtest*`;

$passed = 0;
$total = 0;

foreach (@tests) {
    chomp;
    @lines = `./bin/mmatomax_test.pl $_`;
    foreach (@lines) {
	if (/^\((\d+)\/(\d+)\)/) {
	    $passed += $1;
	    $total += $2;
	}
    }
    print(join('',@lines));
    print "\n"
}

print "\n#######################################\n";
print "Of all tests ($passed/$total) passed.\n";
