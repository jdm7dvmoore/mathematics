#!/usr/bin/perl
use strict;
use MmaToMaxima;
use Data::Compare;
use Data::Dumper;
use Carp;
$Carp::Verbose = 1;

our $Test_name;
our $Top_comment;

require( $ARGV[0]);
#my @testlines = <>;
#my $text = eval(join('',@testlines));

my $mma_code;
my $expected_translation;
my $comment;
my $expected_parse_tree;
my $parse_tree;
my $translation;

our $testcount = 0;
our $alltestcount = 0;
our $parse_succeed = 0;
our $translation_succeed = 0;
our $interpret_succeed = 0;
our $interpret_trys = 0;

our %max_regression_results;

our @tests;

print "======== Comment ===========\n   == ".
    $Top_comment . "\n\n" if defined $Top_comment;
foreach my $test ( @tests ) {

    $alltestcount++;
    $comment = $test->{comment};

    print "======== Test *$Test_name*  $testcount ============\n";
    print "== Comment:\n=== $comment ===\n\n" if $comment;
    if (exists $test->{mma_code} and defined $test->{mma_code} and
            $test->{mma_code} ne '' ) {
	$testcount++;
	$mma_code = $test->{mma_code};
	
	$expected_parse_tree = $test->{expected_parse_tree};
	$expected_translation = $test->{translation};
	
	$parse_tree = MmaToMaxima::ParseMma::parse_string($mma_code);

	print "== Mma code:\n\n";
	print "   $mma_code\n\n";
	print "== Expected Maxima translation:\n\n";
	print "   $expected_translation\n\n";

	
	print "   ";
	if ( not defined $parse_tree ) {
	    print "**** Parse Test $testcount failed to parse\n";
	}
	elsif ( Compare($parse_tree,$expected_parse_tree) ) {
	    print( "== Parse Test $testcount succeeded\n");
	    $parse_succeed++;
	}
	else {
	    print( "** Parse Test  *$Test_name* $testcount failed\n");
	}

#    $translation = max_print_node($expected_parse_tree);
	$translation = MmaToMaxima::translate_tree($parse_tree);
	
	print "   ";    
	if ( not defined $translation ) {
	    print "**** Translation Test  *$Test_name* $testcount failed to translate\n";
	}
	elsif ( $translation eq $expected_translation ) {
	    print( "== Translation Test $testcount succeeded\n");
	    $translation_succeed++;
	}
	else {
	    print( "** Translation Test  *$Test_name*  $testcount failed: got '$translation'\n");
	}
	
	open( OHAND,">transtestcode");
	print OHAND $translation;
	close(OHAND);

	if ( not exists $test->{no_interpret} ) {
	    $interpret_trys++;
	    my $maxima_interpretation_result = `maxima -b transtestcode`;
	    
	    print "   ";    
	    if ( $maxima_interpretation_result =~ /\(%o\d+\)/ ) {
		print( "== Maxima Interpretation Test $testcount succeeded\n");
		$interpret_succeed++;
	    }
	    else {
		print( "** Maxima Interpretation Test $testcount failed\n");
	    }
	}
    }
    if (exists $test->{maxima_tests} and defined $test->{maxima_tests} and
           @{$test->{maxima_tests}} >0 ) {
	print( "== Maxima Regression Test Results: $alltestcount\n");
	write_rtest_files($test);
	my @maxtest = `maxima -b runbatch`;
	
	my $flag = 0;
	foreach( @maxtest ) {
	    next while $flag == 0 and not /Problem 1/;
	    $flag = 1;
	    print "  $_";
	    if ( /(\d+\/\d+) tests passed\./ ) {
		$max_regression_results{$alltestcount} = "($1)";
	    }
	}
    }
    print "\n\n";
}

print "==================================\n";
print "Test *$Test_name* results\n";

if ( $parse_succeed < $testcount )  {
    print "*** Errors: (".( $testcount-$parse_succeed) .
        "/" . $testcount . ") parses failed.\n";
}
else {
    print "(".($parse_succeed) .
        "/" . $testcount . ") parses succeeded.\n";
}


if ( $translation_succeed < $testcount )  {
    print "*** Errors: (".( $testcount-$translation_succeed) .
        "/" . $testcount . ") translations failed.\n";
}
else {
    print "(".($translation_succeed) .
        "/" . $testcount . ") translations succeeded.\n";
}

if ( $interpret_succeed < $interpret_trys )  {
    print "*** Errors: (".( $interpret_trys-$interpret_succeed) .
        "/" . $interpret_trys . ") interprets failed.\n";
}
else {
    print "(".($interpret_succeed) .
        "/" . $interpret_trys . ") interprets succeeded.\n";
}

foreach ( sort {$a<=>$b} keys (%max_regression_results) ) {
    my $result = $max_regression_results{$_};
    print "$result passed in  Maxima regression for Test $_\n";
}

sub write_rtest_files {
    my ($test)  = @_;

    my $rtest = 'rtest_' . $alltestcount; 

    if (defined $translation ) {
	$translation .= ";\n" unless $translation =~ /\;\s*$/;
    }
    open(OHAND,">runbatch") or die "Can't open runbatch test file";
    print OHAND 'load("mmacompat.mac")$' . "\n" . 'display2d:false;' . "\n\n";
    print OHAND $translation . "\n" if defined $translation;
    print OHAND 'batch("' . $rtest . '",test);' . "\n";
    close OHAND;

    open(OHAND,">$rtest") or die "Can't open $rtest test file";
    foreach my $ex (@{$test->{maxima_tests}}) {  # $ex is hash ref
         if (not exists $ex->{result} ) {
              die "Test entry 'maxima_tests' for test $alltestcount contains no 'result'";
         }
          if (exists $ex->{mma_tcode}) {
            my $mma_tcode = $ex->{mma_tcode};
#           print STDERR "** test mma_tcode " . $mma_tcode . "\n";
            my $tree = MmaToMaxima::ParseMma::parse_string($mma_tcode);
#           print STDERR "** tree " . Dumper($tree);
            my $trans = MmaToMaxima::translate_tree($tree);
#           print STDERR "** translation  $trans\n";
            $ex->{finalcode} = $trans .";\n";
            print "  Testing translation of\n\n Mma Code:  $mma_tcode\n\n";
            print "  $trans\n";
            if (exists $ex->{comment}) {
             print " === comment: " .$ex->{comment} . "\n\n";
            }
           }
          else {
             if (not exists $ex->{max_tcode} ) {
               die "Test entry 'maxima_tests' for test $alltestcount contains neither mma_tcode nor max_tcode";
             }
             $ex->{finalcode} = $ex->{max_tcode};
          }
        print OHAND $ex->{finalcode} . "\n"  . $ex->{result} . "\n";
        print OHAND "\n";
    }
    close(OHAND);
}
