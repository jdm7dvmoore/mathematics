#!/usr/bin/perl
use strict;
use MmaToMaxima;
use Data::Dumper;
use Carp;
$Carp::Verbose = 1;

my @lines = <>;
my $string = join('',@lines);


print MmaToMaxima::string_to_maxima_tree_string($string);
print "\n";
