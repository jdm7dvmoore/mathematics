;;Combinatorica functions in Common Lisp
;; author: Richard Fateman with C. Ruan
;; Since there is a choice of data structures that can be used for
;; Mathematica's List  {a,b,c}  ... this could be either
;; a lisp list  (a b c)  or a lisp simple vector  #(a b c).
;; some functions are set to accept either.

;; These are not the simplest functions that can be written. Almost the same
;; deliberately naive techniques could be used as is used in Combinatorica.
;; However, one of the goals in rewriting in Lisp is to provide massively
;; faster programs. By compiling these lisp programs, orders of magnitude
;; speedup should be demonstrable.

(defun range (n &aux ans)
  "Return a list of elements from 1 to n"
  (declare (fixnum n))
  (do ((i n (1- i)))
      ((= i 0) ans)
      (declare (fixnum i))
      (setq ans (cons i ans))))


(defun vectorrange (g)
  "Return a vector of elements from 1 to n"
(declare (fixnum g))
  (let ((ans (make-array g :element-type 'fixnum)))
    (do ((i 1 (1+ i)))
	((> i g) ans)
	(declare (fixnum i))
	(setf (aref ans (1- i)) i))))


(defun permutationslist(bag)
  "Return a list of all the permutations of the input list."
  (if (null bag) '(())
    ;; otherwise take an element, e, out of the bag.
    ;; generate all permutations of the remaining elements.
    ;; and add e to the front of each of these.
    ;; do this for all possible e to generate all permutations.
    (mapcan #'(lambda(e)(mapcar #'(lambda(p)(cons e p))
				(permutationslist
				 (remove e bag :count 1 :test #'eq))))
	    bag)))

(defun permutations(bag)
  "Return a list of all the permutations of the input: vector or list"
  (if (listp bag) (permutationslist bag) 
    (mapcar #'(lambda(r)(coerce r 'vector)) 
	    (permutationslist (coerce bag 'list)))))

(defun permutationQ (p 
		     &aux 
		     (tmp (sort (copy-seq p) #'<)) ;since sort is destructive
		     (len (length p)))
  "True iff p (a vector or list of fixnums)
   is an ordering of the integers 1 to n without repetition"
  (declare (fixnum len))
  (cond ((listp tmp)
	 (do ((i 1 (1+ i))
	      (L tmp (cdr L)))
	     ((> i len) (null L))
	     (declare (fixnum i))
	     (if (= i (car L)) nil (return-from permutationQ nil))))
	(t ; tmp is a vector
	 (do ((i 1 (1+ i)))
	     ((> i len) t)
	     (if (= i (svref tmp (1- i))) nil (return-from permutationQ nil)))
	 )))

(defun permute (l p)
  "Permute l according to the permutation p, assumes l and p have the
   same length."
;; could be done without coerce p to a list if it is a vector,
;; by using elt. But this works..
;; returns a list or a vector depending on what l is.
  (cond ((permutationQ p)
	 (if (listp l)
	     (mapcar #'(lambda (x)
			 (nth (1- x) l))
		     (coerce p 'list))
	     (coerce (mapcar #'(lambda (x)
				 (aref l (1- x)))
			     (coerce p 'list)) 'vector)))	     
	(t (error "~s is not a permutation.~%" p))))


;;......... that's all for now.



