(in-package :maxima)

;(defun $mockmmatest (e) (mma::disp(mma::BuildFormat (mma::|MaxEval| (with-input-from-string (stream e)(mma::p stream))))))
(defun $mockmmatest (e) (setq mmaresult (mma::|MaxEval| (with-input-from-string (stream e)(mma::p stream))))
  (cond 
    ( (equal mmaresult 'mma::|True|)t)
    ( (equal mmaresult 'mma::|False|) nil)
    (t  mmaresult)
    ))
